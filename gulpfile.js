"use strict";
let produccion = false; // puede ser 'true o false

const {src, dest, series, watch} = require('gulp'),
    pug = require('gulp-pug'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    gcmq = require('gulp-group-css-media-queries'),
    cssmin = require('gulp-cssmin'),
    babel = require('gulp-babel'),
    concat = require('gulp-concat'),
    decomment = require('gulp-decomment'),
    sourcemaps = require('gulp-sourcemaps'),
    browsersync = require("browser-sync").create(),
    javascriptObfuscator = require('gulp-javascript-obfuscator'),
    clean = require('gulp-clean'),
    tingpng = require('gulp-tinypng'),
    ts = require('gulp-typescript'),
    tsProject = ts.createProject('tsconfig.json'),

    directorios = {
        html: {
            fuente: 'src/views/**/*.pug',
            destino: 'public/',
            watch: 'public/*.html'
        },
        estilos: {
            fuente: 'src/resources/assets/sass/**/*.scss',
            destino: 'public/css/'
        },
        scripts: {
            fuente: [
                //Jquery
                'node_modules/jquery/dist/jquery.js',

                //Jquery UI
                //'bower_components/jquery-ui/jquery-ui.js',

                //Materialize
                'node_modules/materialize-css/js/cash.js',
                'node_modules/materialize-css/js/component.js',
                'node_modules/materialize-css/js/global.js',
                'node_modules/materialize-css/js/anime.min.js',
                //'node_modules/materialize-css/js/collapsible.js',
                //'node_modules/materialize-css/js/dropdown.js',
                'node_modules/materialize-css/js/modal.js',
                //'node_modules/materialize-css/js/materialbox.js',
                //'node_modules/materialize-css/js/parallax.js',
                //'node_modules/materialize-css/js/tabs.js',
                'node_modules/materialize-css/js/tooltip.js',
                'node_modules/materialize-css/js/waves.js',
                //'node_modules/materialize-css/js/toasts.js',
                //'node_modules/materialize-css/js/sidenav.js',
                //'node_modules/materialize-css/js/scrollspy.js',
                //'node_modules/materialize-css/js/autocomplete.js',
                //'node_modules/materialize-css/js/forms.js',
                //'node_modules/materialize-css/js/slider.js',
                //'node_modules/materialize-css/js/cards.js',
                //'node_modules/materialize-css/js/chips.js',
                //'node_modules/materialize-css/js/pushpin.js',
                'node_modules/materialize-css/js/buttons.js',
                //'node_modules/materialize-css/js/datepicker.js',
                //'node_modules/materialize-css/js/timepicker.js',
                //'node_modules/materialize-css/js/characterCounter.js',
                //'node_modules/materialize-css/js/carousel.js',
                //'node_modules/materialize-css/js/tapTarget.js',
                //'node_modules/materialize-css/js/select.js',
                //'node_modules/materialize-css/js/range.js',

                //Plugins
                //'node_modules/video.js/dist/video.js',
                //'node_modules/three/build/three.js',
                //'node_modules/videojs-panorama/dist/videojs-panorama.v4.js',

                //'node_modules/uevent/browser.js',
                //'node_modules/dot/doT.js',
                //'node_modules/photo-sphere-viewer/dist/photo-sphere-viewer.js',

                //'src/resources/assets/js/panolens.js',

                'bower_components/jquery-smoove/dist/jquery.smoove.js',
                'node_modules/fullpage.js/dist/fullpage.js',
                'node_modules/owl.carousel/dist/owl.carousel.js',
                'public/js/jquery.mousewheel.js',
                'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js',

                //Funciones del proyecto
                'src/resources/assets/js/general.js',
                //'src/resources/assets/js/funciones.js',
            ],
            destino: 'public/js/',
            watch: ['src/resources/assets/js/*.js']
        }
    };

function limpiar() {
    return src(['public/**/*.html', 'public/css/*.css', '*.zip'], {read: false})
        .pipe(clean());
}

function imagenes() {
    return src('src/resources/assets/image/**/*.{jpg, png}')
        .pipe(tingpng('t1Uqiz1WwNciFaHVPxaPy1k5R-3T6SuY'))
        .pipe(dest('public/prueba'));
}

function html() {
    if (produccion) {
        return src(directorios.html.fuente)
            .pipe(pug())
            .pipe(dest(directorios.html.destino))
            .pipe(browsersync.stream());
    } else {
        return src(directorios.html.fuente)
            .pipe(pug({
                pretty: true
            }))
            .pipe(dest(directorios.html.destino))
            .pipe(browsersync.stream());
    }
}

function css() {
    sass.compiler = require('node-sass');

    if (produccion) {
        return src(directorios.estilos.fuente)
            .pipe(sass({style: 'compressed'}).on('error', sass.logError))
            .pipe(autoprefixer({
                cascade: false
            }))
            .pipe(gcmq())
            .pipe(cssmin())
            .pipe(dest(directorios.estilos.destino))
            .pipe(browsersync.stream());
    } else {
        return src(directorios.estilos.fuente)
            .pipe(sass().on('error', sass.logError))
            .pipe(autoprefixer({
                cascade: false
            }))
            .pipe(dest(directorios.estilos.destino))
            .pipe(browsersync.stream());
    }
}

function javascript() {
    if (produccion) {
        return src(directorios.scripts.fuente)
            .pipe(decomment({trim: true}))
            .pipe(babel({
                plugins: [
                    'transform-es2015-arrow-functions',
                    'transform-es2015-block-scoping',
                    'transform-es2015-classes',
                    'transform-es2015-template-literals',
                    'transform-es2015-object-super'
                ]
            }))
            .pipe(concat('scripts.js'))
            .pipe(javascriptObfuscator())
            .pipe(dest(directorios.scripts.destino))
            .pipe(browsersync.stream());
    } else {
        return src(directorios.scripts.fuente)
            .pipe(babel({
                plugins: [
                    'transform-es2015-arrow-functions',
                    'transform-es2015-block-scoping',
                    'transform-es2015-classes',
                    'transform-es2015-template-literals',
                    'transform-es2015-object-super'
                ]
            }))
            .pipe(concat('scripts.js'))
            .pipe(dest(directorios.scripts.destino))
            .pipe(browsersync.stream());
    }
}

function typescript() {
    return tsProject.src()
        .pipe(tsProject())
        .pipe(dest(directorios.scripts.destino))
        .pipe(browsersync.stream());
}

function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: "public",
            serveStaticOptions: {
                extensions: ['html']
            }
        },
        port: 3000
    });
    watch(directorios.estilos.fuente, css);
    watch(directorios.scripts.watch, javascript);
    //watch('src/resources/assets/js/*.ts', typescript);
    watch(directorios.html.fuente, html);

    watch(directorios.html.watch, {awaitWriteFinish: true}, browsersync.reload);
    watch('plubic/js/*.js').on('change', browsersync.reload);

    done();
}

const compilar = series(limpiar, css, javascript, html, browserSync);
//const compilar = series(limpiar, css, typescript, html, browserSync);

exports.html = html;
exports.css = css;
exports.javascript = javascript;
exports.typescript = typescript;
exports.limpiar = limpiar;
exports.imagenes = imagenes;
exports.compilar = compilar;
exports.default = compilar;
