$(function () {
    mapa($('.contenido'), 3000, 2849);
    mapa($('.contenidoPiso5'), 3000, 2184);
    mapa($('.contenidoDibujo'), 3314, 2564);
});

function mapa(elemento, width, height){
    let contenido = elemento,
        ancho = width,
        alto = height;

    contenido.draggable({
        /*appendTo: ".mapaInteractivo",
        //containment: ".mapa",
        snapTolerance: 0,
        stack: ".mapa",*/
        drag: function (event, ui) {
            let anchoVentana = $(window).width();
            let altoVentana = $(window).height();

            /*console.log('ancho ventana: ', anchoVentana);
            console.log('alto ventana: ', altoVentana);
            console.log('no se que busco ', altoVentana - alto);
            console.log(ui.position.top);*/

            if(ui.position.top > 0){
                ui.position.top = 0
            }

            if(ui.position.left > 0){
                ui.position.left = 0
            }

            if(ui.position.top < altoVentana - alto){
                /*console.log('entre');*/
                ui.position.top = (altoVentana - alto)
            }

            if(ui.position.left < anchoVentana - ancho){
                /*console.log('entre');*/
                ui.position.left = (anchoVentana - ancho)
            }
        }
    });
}
