$(function () {
    //materialize();
    //juego();
    mapa();
    //video360();
    //video360dos()
    video360tres();
});

function materialize() {
    $('.modal').modal();
    $('.tooltipped').tooltip();
}

function juego() {
    let elemento,
        dentroFuera = false,
        muebles = $(".draggable"),
        cuartos = $(".droppable"),
        aviso = $("#aviso"),
        intro = $('#introJuego');

    //Empieza cronometro cuando se cierra el modal
    intro.modal({
        onCloseStart: function () {
            let tiempo = {
                    hora: 0,
                    minuto: 0,
                    segundo: 0
                },
                tiempo_corriendo = setInterval(function(){
                    // Segundos
                    tiempo.segundo++;

                    if(tiempo.segundo >= 60){
                        tiempo.segundo = 0;
                        tiempo.minuto++;
                    }

                    // Minutos
                    if(tiempo.minuto >= 60){
                        tiempo.minuto = 0;
                        tiempo.hora++;
                    }

                    if(tiempo.hora === 1){
                        clearInterval(tiempo_corriendo);
                        aviso.html('¡¡¡Tu tiempo ha terminado!!!');

                        //muebles.draggable("disable");
                    }

                    $("#hour").text(tiempo.hora < 10 ? '0' + tiempo.hora : tiempo.hora);
                    $("#minute").text(tiempo.minuto < 10 ? '0' + tiempo.minuto : tiempo.minuto);
                    $("#second").text(tiempo.segundo < 10 ? '0' + tiempo.segundo : tiempo.segundo);
                }, 10);
        }
    });

    //Abre modal al inicial el juego
    intro.modal('open');

    function posicion(posicion){
        dentroFuera = posicion
    }

    //Detecta si se enciman los muebles
    $('.rototeable').droppable({
        tolerance: "touch",
        drop: function( event, ui ) {
            aviso.html('Recuerda que no puedes encimar los muebles');

            elemento.animate({
                top: "0px",
                left: "0px"
            });
        },
        out: function( event, ui ) {
            dentroFuera = false
        }
    });



    //Determina el comportamiento de los muebles
    muebles.draggable({
        appendTo: "#droppable",
        containment: "#juego",
        revert: true,
        snap: true,
        snapTolerance: 0,
        stack: ".draggable",
        start: function (evento, ui) {
            elemento = $(this);
        },
        stop: function (evento, ui) {
            $(this).draggable( "disable" );
            $(this).draggable( "enable" );

            if(dentroFuera){
                aviso.html('Recuerda que los muebles deben ir dentro del cuarto');

                elemento.animate({
                    top: "0px",
                    left: "0px"
                });
            }
        }
    });

    /*$('.rotate').on('click', function () {
        console.log('rotando');


    });*/



    //Determina la rotacion de los muebles
    $('.rototeable').draggable({
        handle: this.find('.rotate'),
        opacity: 0.1,
        helper: 'clone',
        drag: function(event) {
            let // get center of div to rotate
                pw = event.target,
                pwBox = pw.getBoundingClientRect(),
                center_x = (pwBox.left + pwBox.right) / 2,
                center_y = (pwBox.top + pwBox.bottom) / 2,
                // get mouse position
                mouse_x = event.pageX,
                mouse_y = event.pageY,
                radians = Math.atan2(mouse_x - center_x, mouse_y - center_y),
                degree = Math.round((radians * (180 / Math.PI) * -1) + 100);

            console.log(this);

            let rotateCSS = 'rotate(' + (degree - 50) + 'deg)';

            $(this).parents(".draggable").css({
                '-moz-transform': rotateCSS,
                '-webkit-transform': rotateCSS
            });
        },
        stop: function (evento, ui) {
            $(this).draggable( "disable" );
            $(this).draggable( "enable" );

            $(this).parents(".draggable").draggable( "disable" );
            $(this).parents(".draggable").draggable( "enable" );
        }
    });
    //Determina el comportamiento de los cuartos
    cuartos.droppable({
        tolerance: "fit",
        drop: function( event, ui ) {
            elemento.draggable({
                revert: false
            });

            posicion(false);
        },
        out: function( event, ui ) {
            posicion(true)
        }
    });
}

function mapa(){
    let contenido = $('.contenido'),
        ancho = contenido.find('img').width(),
        alto = contenido.find('img').height();

    contenido.draggable({
        /*appendTo: ".mapaInteractivo",
        //containment: ".mapa",
        snapTolerance: 0,
        stack: ".mapa",*/
        drag: function (event, ui) {
            let anchoVentana = $(window).width();
            let altoVentana = $(window).height();

            console.log('ancho ventana: ', anchoVentana);
            console.log('alto ventana: ', altoVentana);
            console.log('no se que busco ', altoVentana - alto);
            console.log(ui.position.top);

            if(ui.position.top > 0){
                ui.position.top = 0
            }

            if(ui.position.left > 0){
                ui.position.left = 0
            }

            if(ui.position.top < altoVentana - alto){
                console.log('entre');
                ui.position.top = (altoVentana - alto)
            }

            if(ui.position.left < anchoVentana - ancho){
                console.log('entre');
                ui.position.left = (anchoVentana - ancho)
            }
        }
    });
}

function video360() {
    function isMobile() {
        var check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    }
    (function(window, videojs) {
        var player = window.player = videojs('videojs-panorama-player', {}, function () {
            window.addEventListener("resize", function () {
                var canvas = player.getChild('Canvas');
                if(canvas) canvas.handleResize();
            });
        });
        var videoElement = document.getElementById("videojs-panorama-player");
        var width = videoElement.offsetWidth;
        var height = videoElement.offsetHeight;
        player.width(width), player.height(height);
        player.panorama({
            clickToToggle: (!isMobile()),
            clickAndDrag: true,
            autoMobileOrientation: true,
            initFov: 100,
            NoticeMessage: (isMobile())? "please move your phone" : "please use your mouse drag and drop the video",
            element:  document.getElementById("hotspot"),
            Markers: [
                {
                    location: {
                        lat: 0,
                        lon: 180
                    },
                    radius: 500,
                    element: "This is text 1 with long text",
                    keyPoint: 0,
                    duration: 2000
                },
                {
                    location: {
                        lat: 20,
                        lon: 160
                    },
                    radius: 500,
                    element: "This is text 2 with long text",
                    onShow: function(){
                        console.log("text 2 is shown");
                    },
                    onHide: function(){
                        console.log("text 2 is hidden");
                    }
                }
            ],
            callback: function () {
                if(!isMobile()) player.play();
            }
        });
    }(window, window.videojs));
}

function video360dos() {
    var viewer = new PhotoSphereViewer({
        container: 'viewer',
        panorama: 'video/shark.mp4'
    });
}

function video360tres() {
    /*const panorama = new PANOLENS.VideoPanorama( 'video/shark.mp4' );
    const viewer = new PANOLENS.Viewer();
    viewer.add( panorama );*/

    var infospot, infospot2, infospot3, panorama, viewer;

    infospot = new PANOLENS.Infospot();
    infospot.position.set( 5000.00, -665.23, -3996.49 );
    infospot.addHoverText( 'Primer marcador' );

    infospot2 = new PANOLENS.Infospot( 300, PANOLENS.DataImage.Info );
    infospot2.position.set( -5000.00, -1825.25, -197.56 );
    infospot2.addHoverElement( document.getElementById( 'desc-container' ), 200 );

    infospot3 = new PANOLENS.Infospot();
    infospot3.position.set(-65.35, 190.11, -5000.00);
    infospot3.addHoverText( 'Tercer marcador' );



    // Get Google Map API Key - https://developers.google.com/maps/documentation/javascript/get-api-key
    //panorama = new PANOLENS.VideoPanorama('video/prueba.mp4', {
    panorama = new PANOLENS.ImagePanorama('video/prueba.jpg', {
        loop: false,
        preload: true
    });
    panorama.add( infospot );
    panorama.add( infospot2 );
    panorama.add( infospot3 );

    viewer = new PANOLENS.Viewer({
        controlBar: false,
        output: 'console'
    });
    viewer.add( panorama );

    panorama.addEventListener('load', function () {
        console.log('Video Listo...');
        setTimeout(function () {
            $('#posicion0').trigger('click')
        }, 2000)
    });

    $('#posicion0').on('click', function () {
        console.log('dando falso click');
        infospot2.hide();
        infospot3.hide();
        $('.cargando').fadeOut('slow')
    });

    $('#posicion1').on('click', function () {
        panorama.pauseVideo();
        panorama.setVideoCurrentTime( {percentage: 0.3}  );
        infospot.show();
        infospot2.hide();
        infospot3.hide();
    });

    $('#posicion2').on('click', function () {
        panorama.pauseVideo();
        panorama.setVideoCurrentTime( {percentage: 0.6}  );
        infospot.hide();
        infospot2.show();
        infospot3.hide();
    });

    $('#posicion3').on('click', function () {
        panorama.setVideoCurrentTime( {percentage: 0.8}  );
        panorama.playVideo();
        infospot.hide();
        infospot2.hide();
        infospot3.show();
    });
}
