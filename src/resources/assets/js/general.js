'use strict';

$(function () {
    /* Funciones de Materialize */
    materialize();

    /* Muestra u oculta menu principal */
    menu();

    /* Muestra u oculta menu del footer */
    menuFooter();

    /* Desactiva o activa el audio de toda la pagina */
    silencio();

    /* Funciones que solo ejecutan en el home */
    rutas.agregar('home', function () {
        desactivarMenuPrincipal();
        desactivarMenuFooter();
        animacionHome();
        saltar('comenzar');
    });

    /* Funciones que solo ejecutan en el intro */
    rutas.agregar('intro', function () {
        desactivarMenuFooter();
        saltar('saltar');
        seRenta();
    });

    rutas.agregar('anuncio', function () {
        desactivarMenuFooter();
        finalizarVideo('anuncio', finalizaIntro);
        saltaAnuncio();
    });

    rutas.agregar('intro-elevador', function () {
        desactivarMenuFooter();
        finalizarVideo('elevador', saltarVideoIntroElevador);
        botonSaltarVideoIntroElevador()
    });

    rutas.agregar('elevador', function () {
        paginaElevador();
        botonesElevedor();
        mapaInteractivo();
        sliderModalMapaInteractivoElevador();
        sliderAnosElevador();
        sliderModalMapa360();
        modalAnos();
        imagen360();
        sliderPiso5slider();
        animacionpiso5();
        mapaSonoroBoton();
        audioMapaSonoro();
    });

    rutas.agregar('acerca-de-ermitanos', function () {
        animacionAcercaDeErmitanos()
    });

    rutas.agregar('descansar', function () {
        animacionDescansar()
    });

    rutas.agregar('chat', function () {
        chat();
        galeriaChat();
    });

    rutas.agregar('juego', function () {
        juego();
    });

    rutas.correr();
});

/*
* Controla lo que ejecuta en cada pagina
* */
var rutas = {
    _rutas : {},
    agregar: function (url, accion) {
        this._rutas[url] = accion
    },
    correr: function () {
        $.each(this._rutas, function (patron) {
            if($('main').hasClass(patron)) {
                this();
            }
        })
    }
};

var animacion, animaciondos, animaciontres;

/*
* Funciones globales
* */
function materialize() {
    $('.modal').modal();

    $('#empezar').modal({
        opacity: 0.2,
        endingTop: 0,
        dismissible: false,
        onOpenEnd: function () {
            animacion.pause();
            animaciondos.pause()
        },
        onCloseEnd: function () {
            animacion.play();
            animaciondos.play();
        }
    });

    $('#modalAnuncio').modal({
        opacity: 0.2,
        endingTop: 0,
        dismissible: false
    });

    $('#modalMapaInteractivo').modal({
        opacity: 0.2,
        endingTop: 0,
        dismissible: false
    });

    $('#modalAnos').modal({
        opacity: 0.8,
        endingTop: 0,
        dismissible: false
    });

    $('#introJuego').modal({
        opacity: 0.8,
        endingTop: 0,
        dismissible: false
    });

    $('#finaljuego').modal({
        opacity: 0.8,
        endingTop: 0,
        dismissible: false,
        onCloseEnd: function () {
            $('.simulacion-slider').addClass('active');

            setTimeout(function () {
                $('.simulacion-slider').html(`
                    <div class="texto">
                        <p class="fadeInDown animated">Su arquitecto Juan Segura fue visionario al diseñar en 1929 un edificio con 72 departamentos —60 son unipersonales, seis tienen dos habitaciones y sólo 12 son familiares—, además de un cine en la planta baja y diversos locales comerciales. </p>

                        <p class="fadeInDown animated">Sin duda, un modelo vanguardista para su época.</p>
                    </div>
                    
                    <div class="imagen">
                        <img class="fadeIn animated" src="imagenes/juego/paso-2.jpg" alt="paso 2">
                    </div>
                `)
            }, 12000);

            setTimeout(function () {
                $('.simulacion-slider').html(`
                    <div class="texto">
                        <p class="fadeInDown animated">A lo largo de ocho décadas, este inmueble ha sido testigo de la transformación de la ciudad y su sociedad. El Edificio Ermita actualmente es habitado en su mayoría por jóvenes solteros que están postergando la edad para casarse y formar una familia. </p>
                    </div>
                    
                    <div class="imagen">
                        <img class="fadeIn animated" src="imagenes/juego/paso-3.jpg" alt="paso 2">
                    </div>
                `)
            }, 24000);

            setTimeout(function () {
                $('.simulacion-slider').html(`
                    <div class="texto">
                        <p class="fadeInDown animated">¡Bienvenido al Edificio Ermita!<br> Ahora, como el noveno ermitaño, te invitamos a que sigas explorando el edificio y su historia, y a que conozcas tu barrio y a tus vecinos.<br>Pero antes, tómate un descanso.</p>
                    </div>
                `)
            }, 36000);

            setTimeout(function () {
                window.location.href = 'descansar'
            }, 48000)
        }
    });
}

function menu() {
    let boton = $('#menu');

    boton.on('click', function (evento) {
        evento.preventDefault();

        $('header').toggleClass('active')
    })
}

function menuFooter() {
    let boton = $('#menu-footer');

    boton.on('click', function (evento) {
        evento.preventDefault();

        $(this).find('.icono').toggleClass('active');
        $('footer .menu-secundario').fadeToggle('slow')
    })
}

function silencio() {
    let boton = $('#silencio'),
        botonPrincipal = $('#activarAudio');

    boton.on('click', function(evento){
        evento.preventDefault();

        $(this).find('.icono').toggleClass('active');

        if($(this).find('.icono').hasClass('active')){
            $('audio, video').each(function(){
                $(this).prop('muted', true)
            });
        } else {
            $('audio, video').each(function(){
                $(this).prop('muted', false)
            });
        }
    });

    botonPrincipal.on('click', function(evento){
        evento.preventDefault();

        $(this).toggleClass('active');

        if($(this).hasClass('active')){
            $(this).find('img').attr('src', 'imagenes/desactivar-audio.jpg');

            $('audio, video').each(function(){
                $(this).prop('muted', false)
            });
        }else{
            $(this).find('img').attr('src', 'imagenes/activar-audio.jpg');

            $('audio, video').each(function(){
                $(this).prop('muted', true)
            });
        }
    })
}

function desactivarMenuFooter() {
    $('footer .menu-secundario').hide();
    $('#menu-footer').find('.menu-footer').addClass('active');
}

function desactivarMenuPrincipal() {
    $('header').hide();
}

function animacionAcercaDeErmitanos() {
    $("p").smoove({
        offset  : '30%'
    });

    $("img").smoove()
}

function animacionHome() {
    let animacion = bodymovin.loadAnimation({
        container: document.getElementById('animacion'),
        render: 'svg',
        loop: true,
        autoplay: true,
        path: 'animaciones/presentacion/data.json'
    })
}

function saltar(nombreBoton) {
    let boton = $(`#${nombreBoton}`),
        url = boton.attr('href'),
        lienzo = $('main');

    boton.on('click', function (evento) {
        evento.preventDefault();

        lienzo.animate({opacity: '0'}, 2000, function(){
            window.location.href = url
        });
    })
}

/*
* Detecta cuando termina un video
* */
function finalizarVideo(nombreVideo, accion) {
    let video = $(`#${nombreVideo}`);

    video.on('ended', accion)
}

function finalizaIntro() {
    let url = $('#saltar').attr('href');

    window.location.href = url
}

function seRenta(){
    let ventana = $('#empezar');

    setTimeout(function () {
        ventana.modal('open')
    }, 30000);
}

function mostrarModalEnAnuncio() {
    let ventana = $('#modalAnuncio');

    ventana.modal('open')
}

function saltaAnuncio() {
    let boton = $('#saltar'),
        video = document.getElementsByTagName("video")[0];

    boton.on('click', function (evento) {
        evento.preventDefault();

        //video.pause();

        window.location.href = $(this).attr('href')

        //mostrarModalEnAnuncio();
    })
}

function saltarVideoIntroElevador() {
    let url = $('#saltarVideo').attr('href'),
        lienzo = $('main');

    lienzo.animate({opacity: 0}, 1000, function(){
        window.location.href = url;
    })
}

function botonSaltarVideoIntroElevador() {
    let boton = $('#saltarVideo'),
        video = document.getElementById('elevador');

    boton.on('click', function(evento){
        evento.preventDefault();
        console.log(video.currentTime);

        if(video.currentTime > 0 && video.currentTime < 5){
            video.currentTime = boton.attr('data-posicion');
        }

        if(video.currentTime > 5 && video.currentTime < 35){
            video.currentTime = 35;
        }

        if(video.currentTime > 35){
            window.location.href = boton.attr('href');
        }
    })
}

function paginaElevador() {
    let lector = $('#lector span'),
        contador = 0,
        contadorDos = 0,
        contadorTres = 0;

    $('#fullpage').fullpage({
        autoScrolling: true,
        scrollHorizontally: true,
        scrollingSpeed: 1500,
        keyboardScrolling: false,
        afterLoad: function(origin, destination, direction){
            if(destination.anchor !== 'piso0'){
                $('#menuPisos').addClass('active');

                switch (destination.anchor) {
                    case 'piso1':
                        fullpage_api.setAllowScrolling(false, 'down');
                        lector.html('Piso 1');
                        $('#audioPiso5')[0].pause();
                        break;
                    case 'piso2':
                        fullpage_api.setAllowScrolling(false, 'down');
                        lector.html('Piso 2');

                        let video = $('#juanSegura')[0];
                        video.play();
                        $('#audioPiso5')[0].pause();

                        break;
                    case 'piso3':
                        fullpage_api.setAllowScrolling(false, 'down');
                        lector.html('Piso 3');

                        contadorDos++;

                        if(contadorDos === 1){
                            $('#modal360').modal('open');
                        }
                        $('#audioPiso5')[0].pause();
                        break;
                    case 'piso4':
                        fullpage_api.setAllowScrolling(false, 'down');
                        contador++;

                        lector.html('Piso 4');
                        if(contador === 1){
                            console.log('entre por primera vez');
                            $('#modalMapaInteractivo').modal('open');
                        }
                        $('#audioPiso5')[0].pause();
                        break;
                    case 'piso5':
                        fullpage_api.setAllowScrolling(false, 'down');
                        lector.html('Piso 5');

                        contadorTres++;

                        if(contadorTres === 1){
                            $('#modalpiso5').modal('open');
                        }

                        $('#audioPiso5')[0].play();

                        break;
                    case 'piso6':
                        lector.html('Piso 6');
                        fullpage_api.setAllowScrolling(true, 'down');
                        $('#audioPiso5')[0].pause();
                        break;
                    default:
                        lector.html('Piso');
                }
            }else{
                $('#menuPisos').removeClass('active');
            }
        }
    });

    fullpage_api.setAllowScrolling(false, 'up');
    fullpage_api.setAllowScrolling(false, 'down');

    $('#pisoAnterior').on('click', function (evento) {
        evento.preventDefault();

        $('.contenido, .contenidoDibujo, .contenidoPiso5').animate({top: '0', left: '0'}, 500);

        fullpage_api.moveSectionUp()
    });

    $('#pisoSiguiente').on('click', function (evento) {
        evento.preventDefault();

        $('.contenido, .contenidoDibujo, .contenidoPiso5').animate({top: '0', left: '0'}, 500);

        fullpage_api.moveSectionDown()
    })
}

function botonesElevedor() {
    $('#menuElevador').find('a').on('mouseover', function () {
        let titulo = $(this).attr('data-contenido');

        $('.puerta .info').removeClass('active');

        $(`.puerta .info.${titulo}`).addClass('active')
    })
}

function mapaInteractivo(){
    animaciontres = bodymovin.loadAnimation({
        container: document.getElementById('animacionMapa'),
        render: 'svg',
        loop: true,
        autoplay: true,
        path: 'animaciones/elevador/data.json'
    })
}

function mapaSonoroBoton(){
    $('#mostrarMapaSonoro').on('click', function(){
        $('#mapaSonoro').addClass('active');
        $('#audioPiso5')[0].pause();
    });

    $('#ocultarMapaSonoro').on('click', function(){
        $('#mapaSonoro').removeClass('active');
        $('#mapaSonoro').find('audio')[0].pause();
        $('#audioPiso5')[0].play();
    })
}

function audioMapaSonoro(){
    $('#mapaSonoro svg.audio').on('click', function(){
        console.log('le di click');
        $('#mapaSonoro').find('audio')[0].pause();

        $(this).siblings('audio')[0].play();
    })
}

function animacionpiso5(){
    animaciontres = bodymovin.loadAnimation({
        container: document.getElementById('animacionPiso5'),
        render: 'svg',
        loop: true,
        autoplay: true,
        path: 'animaciones/elevador/piso5/data.json'
    })
}

function sliderModalMapaInteractivoElevador() {
    let svg = `<span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.67493 9.09525">
                        <path d="M5.7991,8.99059.23086,4.99729a.55336.55336,0,0,1,0-.89933L5.7991.10466a.55335.55335,0,0,1,.87583.44966V8.54093A.55335.55335,0,0,1,5.7991,8.99059Z"/>
                    </svg>
                </span>`;

    $("#presentacion").owlCarousel({
        items: 1,
        nav: true,
        dots: true,
        navText: [svg, svg]
    });
}

function sliderModalMapa360() {
    let svg = `<span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.67493 9.09525">
                        <path d="M5.7991,8.99059.23086,4.99729a.55336.55336,0,0,1,0-.89933L5.7991.10466a.55335.55335,0,0,1,.87583.44966V8.54093A.55335.55335,0,0,1,5.7991,8.99059Z"/>
                    </svg>
                </span>`;

    $("#modal360slider").owlCarousel({
        items: 1,
        nav: true,
        dots: true,
        navText: [svg, svg]
    });
}
function sliderPiso5slider() {
    let svg = `<span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.67493 9.09525">
                        <path d="M5.7991,8.99059.23086,4.99729a.55336.55336,0,0,1,0-.89933L5.7991.10466a.55335.55335,0,0,1,.87583.44966V8.54093A.55335.55335,0,0,1,5.7991,8.99059Z"/>
                    </svg>
                </span>`;

    $(" #modalPiso5slider").owlCarousel({
        items: 1,
        nav: true,
        dots: true,
        navText: [svg, svg]
    });
}

function sliderAnosElevador() {
    let svg = `<span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.67493 9.09525">
                        <path d="M5.7991,8.99059.23086,4.99729a.55336.55336,0,0,1,0-.89933L5.7991.10466a.55335.55335,0,0,1,.87583.44966V8.54093A.55335.55335,0,0,1,5.7991,8.99059Z"/>
                    </svg>
                </span>`,
        sliderInformacion = $("#anosInformacion");

    $("#anos").owlCarousel({
        items: 4,
        nav: true,
        dots: true,
        navText: [svg, svg],
        stagePadding: 30,
        responsive: {
            1280: {
                items: 6
            }
        }
    });

    sliderInformacion.owlCarousel({
        items: 1,
        nav: false,
        autoHeight:true
    });

    $('#anoAnteriorModal').on('click', function(evento){
        evento.preventDefault();

        sliderInformacion.trigger('prev.owl.carousel');
    });

    $('#anoSiguienteModal').on('click', function(evento){
        evento.preventDefault();

        sliderInformacion.trigger('next.owl.carousel');
    });

    sliderInformacion.on('changed.owl.carousel', function(evento) {
        console.log(evento.relatedTarget.current());
        let ano = $('#contadorAnos span'),
            anterior = $('#anoAnteriorModal'),
            siguiente = $('#anoSiguienteModal'),
            controladores = $('#menuAnosModal li a');

        switch (evento.relatedTarget.current()) {
            case 0:
                ano.html('1868');
                anterior.addClass('disabled');
                break;
            case 1:
                ano.html('1870');
                controladores.removeClass('disabled');
                break;
            case 2:
                ano.html('1872');
                controladores.removeClass('disabled');
                break;
            case 3:
                ano.html('1873');
                controladores.removeClass('disabled');
                break;
            case 4:
                ano.html('1885');
                controladores.removeClass('disabled');
                break;
            case 5:
                ano.html('1898');
                controladores.removeClass('disabled');
                break;
            case 6:
                ano.html('1899');
                controladores.removeClass('disabled');
                break;
            case 7:
                ano.html('1907');
                controladores.removeClass('disabled');
                break;
            case 8:
                ano.html('1913');
                controladores.removeClass('disabled');
                break;
            case 9:
                ano.html('1917');
                controladores.removeClass('disabled');
                break;
            case 10:
                ano.html('1929');
                controladores.removeClass('disabled');
                break;
            case 11:
                ano.html('1930');
                controladores.removeClass('disabled');
                break;
            case 12:
                ano.html('1933');
                controladores.removeClass('disabled');
                break;
            case 13:
                ano.html('1935');
                controladores.removeClass('disabled');
                break;
            case 14:
                ano.html('1936');
                controladores.removeClass('disabled');
                break;
            case 15:
                ano.html('1940');
                controladores.removeClass('disabled');
                break;
            case 16:
                ano.html('1960');
                controladores.removeClass('disabled');
                break;
            case 17:
                ano.html('2020');
                siguiente.addClass('disabled');
                break;
        }
    });




    //Animaciones
    function animacion(id, json){
        let animacion = bodymovin.loadAnimation({
            container: document.getElementById(id),
            render: 'svg',
            loop: true,
            autoplay: true,
            path: json
        })
    }

    animacion('animacion1868', 'animaciones/elevador/piso1/1868/data.json');
    animacion('animacion1870', 'animaciones/elevador/piso1/1870/data.json');
    animacion('animacion1872', 'animaciones/elevador/piso1/1872/data.json');
    animacion('animacion1873', 'animaciones/elevador/piso1/1873/data.json');
    animacion('animacion1885', 'animaciones/elevador/piso1/1885/data.json');
    animacion('animacion1898', 'animaciones/elevador/piso1/1898/data.json');
    animacion('animacion1899', 'animaciones/elevador/piso1/1899/data.json');
    animacion('animacion1907', 'animaciones/elevador/piso1/1907/data.json');
    animacion('animacion1913', 'animaciones/elevador/piso1/1913/data.json');
    animacion('animacion1917', 'animaciones/elevador/piso1/1917/data.json');
    animacion('animacion1929', 'animaciones/elevador/piso1/1929/data.json');
    animacion('animacion1930', 'animaciones/elevador/piso1/1930/data.json');
    animacion('animacion1933', 'animaciones/elevador/piso1/1933/data.json');
    animacion('animacion1935', 'animaciones/elevador/piso1/1935/data.json');
    animacion('animacion1936', 'animaciones/elevador/piso1/1936/data.json');
    animacion('animacion1940', 'animaciones/elevador/piso1/1940/data.json');
    animacion('animacion1960', 'animaciones/elevador/piso1/1960/data.json');
    animacion('animacion2020', 'animaciones/elevador/piso1/2020/data.json');
}

function modalAnos() {
    $('section.piso1').on('click', 'a.circulo', function (evento) {
        evento.preventDefault();

        $('#anosInformacion').trigger('to.owl.carousel', Number($(this).attr('href')));

        $('#modalAnos').modal('open')
    })
}

function chat() {
    let url = 'js/chat.json',
        contador = 1;

    $('#empezarChat').modal('open');

    async function mensaje(numero) {
        let respuesta = await fetch(url),
            data = await respuesta.json();

        return data.mensajes[numero];
    }

    var rtime = new Date();
    var timeout = false;
    var delta = 500;

    function terminaScroll() {
        if (new Date() - rtime < delta) {
            setTimeout(terminaScroll, delta);
        } else {
            timeout = false;
            contador ++;

            mensaje(contador)
                .then(data => {
                    if(data !== undefined && !$('body').hasClass('activo')){
                        $('#chat').append(`
                            <div class="mensaje">
                                <div class="icono">
                                    <img src="imagenes/chat/ermitanos/${data.imagen}" alt="${data.nombre}">
                                </div>
                                
                                <div class="texto">
                                    <div class="informacion">
                                    <p>${rtime.getHours()}:${rtime.getMinutes()}<br>${data.nombre}</p>
                                </div>
                                
                                <div class="mensaje-texto">
                                  <p>${data.mensaje}</p>
                                </div>
                              </div>
                            </div>
                        `);

                        $("body, html").animate({ scrollTop: $('#chat').prop("scrollHeight")}, 1000);
                    }
                });

            $('.cargando-mensaje').removeClass('active');
        }
    }

    $(window).on("mousewheel", function(e){
        if(e.deltaY === -1){
            mensaje(contador + 1)
                .then(data => {
                    if(data !== undefined){
                        $('.cargando-mensaje p').html(`${data.nombre} esta escribiendo...`);
                        $('.cargando-mensaje').addClass('active');
                    }
                });

            rtime = new Date();

            if (timeout === false) {
                timeout = true;
                setTimeout(terminaScroll, delta);
            }
        }
    });
}


function animacionDescansar() {
    let animacion = bodymovin.loadAnimation({
        container: document.getElementById('animacionDescansar'),
        render: 'svg',
        loop: true,
        autoplay: true,
        path: 'animaciones/descansar/data.json'
    }),
        puerta = $('.puerta'),
        celular = $('.celular'),
        bano = $('.bano'),
        gato = $('.gato');


    puerta.on('click',function(){
        console.log('entre');
        window.location.href = "elevador"
    });

    celular.on('click',function(){
        console.log('entre');
        window.location.href = "chat"
    });

    puerta.on('mouseover',function(){
        $(this).find('audio')[0].play();
    });

    celular.on('mouseover',function(){
        $(this).find('audio')[0].play();
    });

    bano.on('mouseover', function(){
        $(this).find('audio')[0].play();
    });

    gato.on('mouseover', function(){
        $(this).find('audio')[0].play();
    });

    /*$('body').on('mouseout', '.psd', function(){
        $('#animacionDescansar').animate({opacity: 1}, 500)
    });*/
}

function imagen360(){
    var panorama, panorama2, panorama3, panorama4, panorama5, panorama6, panorama7, panorama8, panorama9,
        viewer, container,
        infospot, infospot2, infospot3, infospot4, infospot5, infospot6, infospot7, infospot8, infospot9, infospot10, infospot11, infospot12, infospot13, infospot14, infospot15, infospot16, infospot17, infospot18, infospot19, infospot20, infospot21, infospot22, infospot23;

    container = document.querySelector( '#container' );

    //Imagenes
    panorama = new PANOLENS.ImagePanorama( 'imagenes/elevador/piso-3/uno.jpg' );
    panorama2 = new PANOLENS.ImagePanorama( 'imagenes/elevador/piso-3/dos.jpg' );
    panorama3 = new PANOLENS.ImagePanorama( 'imagenes/elevador/piso-3/tres.jpg' );
    panorama4 = new PANOLENS.ImagePanorama( 'imagenes/elevador/piso-3/cuatro.jpg' );
    panorama5 = new PANOLENS.ImagePanorama( 'imagenes/elevador/piso-3/cinco.jpg' );
    panorama6 = new PANOLENS.ImagePanorama( 'imagenes/elevador/piso-3/seis.jpg' );
    panorama7 = new PANOLENS.ImagePanorama( 'imagenes/elevador/piso-3/siete.jpg' );
    panorama8 = new PANOLENS.ImagePanorama( 'imagenes/elevador/piso-3/ocho.jpg' );
    panorama9 = new PANOLENS.ImagePanorama( 'imagenes/elevador/piso-3/nueve.jpg' );

    //Puntos
    infospot = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot.position.set( 5000.00, -455.24, -2098.46 );
    infospot.addHoverText( "Avanzar" );

    infospot.addEventListener( 'click', function(){
        viewer.setPanorama( panorama2 );
    } );

    infospot2 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot2.position.set( -5000.00, -190.37, 1410.25 );
    infospot2.addHoverText( "Subir" );

    infospot2.addEventListener( 'click', function(){
        viewer.setPanorama( panorama3 );
    } );

    infospot3 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot3.position.set( 5000.00, 4780.94, -3771.61 );
    infospot3.addHoverText( "Subir" );

    infospot3.addEventListener( 'click', function(){
        viewer.setPanorama( panorama3 );
    } );

    infospot4 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot4.position.set( -5000.00, -118.15, 872.62 );
    infospot4.addHoverText( "Regresar" );

    infospot4.addEventListener( 'click', function(){
        viewer.setPanorama( panorama );
    } );

    infospot5 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot5.position.set( 5000.00, -38.14, 1199.32 );
    infospot5.addHoverText( "Avanzar" );

    infospot5.addEventListener( 'click', function(){
        viewer.setPanorama( panorama4 );
    } );

    infospot6 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot6.position.set( -5000.00, -442.80, -119.90 );
    infospot6.addHoverText( "Regresar" );

    infospot6.addEventListener( 'click', function(){
        viewer.setPanorama( panorama2 );
    } );

    infospot7 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot7.position.set( 5000.00, -108.14, -2318.69 );
    infospot7.addHoverText( "Avanzar" );

    infospot7.addEventListener( 'click', function(){
        viewer.setPanorama( panorama5 );
    } );

    infospot8 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot8.position.set( 5000.00, -88.19, 655.54 );
    infospot8.addHoverText( "Entrar" );

    infospot8.addEventListener( 'click', function(){
        viewer.setPanorama( panorama6 );
    } );

    infospot9 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot9.position.set( 5000.00, 288.52, 3511.37 );
    infospot9.addHoverText( "Entrar" );

    infospot9.addEventListener( 'click', function(){
        viewer.setPanorama( panorama6 );
    } );

    infospot10 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot10.position.set( -4140.49, -5000.00, 3730.83 );
    infospot10.addHoverText( "Patio" );

    infospot10.addEventListener( 'click', function(){
        viewer.setPanorama( panorama );
    } );

    infospot11 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot11.position.set( -4079.11, 18.37, 5000.00 );
    infospot11.addHoverText( "Regresar Sur" );

    infospot11.addEventListener( 'click', function(){
        viewer.setPanorama( panorama3 );
    } );

    infospot12 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot12.position.set( -1020.32, 46.97, -5000.00 );
    infospot12.addHoverText( "Regresar Norte" );

    infospot12.addEventListener( 'click', function(){
        viewer.setPanorama( panorama4 );
    } );

    infospot13 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot13.position.set( -5000.00, -42.35, -2066.60 );
    infospot13.addHoverElement( document.getElementById( 'primerVideo' ), 200 );

    infospot14 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot14.position.set( 5000.00, -2432.51, -86.63 );
    infospot14.addHoverText( "Salir" );

    infospot14.addEventListener( 'click', function(){
        viewer.setPanorama( panorama5 );
    } );

    infospot15 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot15.position.set( 908.24, 112.85, -5000.00 );
    infospot15.addHoverElement( document.getElementById( 'segundoVideo' ), 200 );

    infospot16 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot16.position.set( -5000.00, -955.65, -1253.49 );
    infospot16.addHoverText( "Avanzar" );

    infospot16.addEventListener( 'click', function(){
        viewer.setPanorama( panorama8 );
    } );

    infospot17 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot17.position.set( 5000.00, -186.90, -1446.76 );
    infospot17.addHoverText( "Cocina" );

    infospot17.addEventListener( 'click', function(){
        viewer.setPanorama( panorama7 );
    } );

    infospot18 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot18.position.set( -1971.18, -642.20, 5000.00 );
    infospot18.addHoverText( "Avanza" );

    infospot18.addEventListener( 'click', function(){
        viewer.setPanorama( panorama6 );
    } );

    infospot19 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot19.position.set( 4261.02, -640.83, 5000.00 );
    infospot19.addHoverText( "Regresar" );

    infospot19.addEventListener( 'click', function(){
        viewer.setPanorama( panorama6 );
    } );

    infospot21 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot21.position.set( 5000.00, -325.68, 213.05 );
    infospot21.addHoverText( "WC" );

    infospot21.addEventListener( 'click', function(){
        viewer.setPanorama( panorama9 );
    } );

    infospot22 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot22.position.set( -1206.80, -480.98, 5000.00 );
    infospot22.addHoverText( "Regresar" );

    infospot22.addEventListener( 'click', function(){
        viewer.setPanorama( panorama8 );
    } );

    infospot20 = new PANOLENS.Infospot( 500, PANOLENS.DataImage.Info );
    infospot20.position.set( 4890.19, 114.59, -5000.00 );
    infospot20.addHoverElement( document.getElementById( 'primerVideo' ), 200 );

    panorama.add( infospot, infospot2 );
    panorama2.add( infospot3, infospot4 );
    panorama3.add( infospot5, infospot6 );
    panorama4.add( infospot7, infospot8 );
    panorama5.add( infospot9, infospot10, infospot11, infospot12);
    panorama6.add( infospot14, infospot15, infospot16, infospot17 );
    panorama7.add( infospot18 );
    panorama8.add( infospot19, infospot20, infospot21 );
    panorama9.add( infospot22 );

    viewer = new PANOLENS.Viewer( {
        container: container,
        output: 'console',
        controlBar: false,             // Vsibility of bottom control bar
        controlButtons: [],            // Buttons array in the control bar. Default to ['fullscreen', 'setting', 'video']
        autoHideControlBar: false,        // Auto hide control bar
        autoHideInfospot: true,            // Auto hide infospots
        horizontalView: false,            // Allow only horizontal camera control
        cameraFov: 60,                // Camera field of view in degree
        reverseDragging: false,            // Reverse orbit control direction
        enableReticle: false,            // Enable reticle for mouseless interaction
        dwellTime: 1500,            // Dwell time for reticle selection in millisecond
        autoReticleSelect: true,        // Auto select a clickable target after dwellTime
        viewIndicator: false,            // Adds an angle view indicator in upper left corner
        indicatorSize: 30,
    });
    viewer.add( panorama, panorama2, panorama3, panorama4, panorama5, panorama6, panorama7, panorama8, panorama9 );

    viewer.addUpdateCallback(function(){

    });

    $('#verMapaPisos, #ocultarMapaPisos').on('click', function(evento){
        evento.preventDefault();

        console.log('dando click');

        $('.contenidoPisos').toggleClass('activo')
    })
}

function galeriaChat() {
    console.log('entrando a chat');

    $('body').on('click', '#mostrarGaleria', function (evento) {
        evento.preventDefault();

        scrollGaleria()
    });

    $('#ocultarGaleria').on('click', function (evento) {
        evento.preventDefault();

        scrollGaleria()
    });

    function scrollGaleria() {
        $("body, html").animate({ scrollTop: $('#chat').prop("scrollHeight")}, 1000);

        $('.galeria').toggleClass('activo');
        $('body').toggleClass('activo');
    }
}





function juego() {
    let elemento,
        dentroFuera = false,
        muebles = $(".draggable"),
        cuartos = $(".droppable"),
        aviso = $("#aviso"),
        intro = $('#introJuego');

    //Empieza cronometro cuando se cierra el modal
    intro.modal({
        onCloseStart: function () {
            let tiempo = {
                    hora: 0,
                    minuto: 60,
                    segundo: 60
                },
                tiempo_corriendo = setInterval(function(){
                    // Segundos
                    tiempo.segundo--;

                    if(tiempo.segundo === 0){
                        tiempo.segundo = 60;
                        tiempo.minuto--;
                    }

                    // Minutos
                    if(tiempo.minuto === -1){
                        tiempo.minuto = 0;
                        $('#finaljuego').modal('open');
                        clearInterval(tiempo_corriendo);
                    }
                    $("#minute").text(tiempo.minuto < 10 ? '0' + tiempo.minuto : tiempo.minuto);
                }, 10);
        }
    });

    //Abre modal al inicial el juego
    intro.modal('open');

    function posicion(posicion){
        dentroFuera = posicion
    }

    //Detecta si se enciman los muebles
    $('.draggable').droppable({
        tolerance: "touch",
        drop: function( event, ui ) {
            aviso.html('Recuerda que no puedes encimar los muebles');

            let precio = Number($('#precio').html());

            $('#precio').html(precio + Number($(elemento).attr('data-precio')));

            elemento.animate({
                top: "0px",
                left: "0px"
            });

            setTimeout(function(){$(ui.draggable).draggable('enable'); }, 1000);
        },
        out: function( event, ui ) {
            dentroFuera = false;
            $(ui.draggable).draggable('enable');
        }
    });

    //Determina el comportamiento de los muebles
    muebles.draggable({
        appendTo: ".droppable",
        containment: ".piso-interno",
        revert: true,
        snap: true,
        snapTolerance: 0,
        stack: ".draggable",
        start: function (evento, ui) {
            elemento = $(this);
            $("html, body").animate({scrollTop: 0}, 500);

            let cantidad = $('#precio'),
                precio = Number(cantidad.html());

            cantidad.addClass('animated fadeInUp');

            cantidad.html(precio - Number($(this).attr('data-precio')));

            $('#sonidoMueble')[0].play();

            setTimeout(function () {
                cantidad.removeClass('animated fadeInUp');
            }, 500);
        },
        stop: function (evento, ui) {
            if(dentroFuera){
                aviso.html('Recuerda que los muebles deben ir dentro del cuarto');

                $(this).attr('data-descuento', "true");

                if(!$(this).hasClass('adentro')){
                    let cantidad = $('#precio'),
                        precio = Number(cantidad.html());

                    cantidad.addClass('animated fadeInUp');

                    cantidad.html(precio + Number($(this).attr('data-precio')));
                    $(ui.draggable).draggable('enable');

                    setTimeout(function () {
                        cantidad.removeClass('animated fadeInUp');
                    }, 500);
                }

                elemento.animate({
                    top: "0px",
                    left: "0px"
                });
            }
        }
    });

    $('.rotar').on('click', function () {
        $(this).parents(".menu").siblings(".draggable").css({transform: "rotate(" + $(this).parents(".menu").siblings(".draggable").attr('data-rotate') + "deg)"});

        $(this).parents(".menu").siblings(".draggable").attr('data-rotate', Number($(this).parents(".menu").siblings(".draggable").attr('data-rotate')) + 45)
    });

    //Determina el comportamiento de los cuartos
    cuartos.droppable({
        tolerance: "fit",
        drop: function( event, ui ) {
            elemento.draggable({
                revert: false
            });

            $(ui.draggable).draggable('disable');

            $('#soltarMueble')[0].play();

            posicion(false);
        },
        out: function( event, ui ) {
            posicion(true);

            $(ui.draggable).removeClass('adentro');
        }
    });

    let svg = `<span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.67493 9.09525">
                        <path d="M5.7991,8.99059.23086,4.99729a.55336.55336,0,0,1,0-.89933L5.7991.10466a.55335.55335,0,0,1,.87583.44966V8.54093A.55335.55335,0,0,1,5.7991,8.99059Z"/>
                    </svg>
                </span>`;

    $("#modalJuegoSlider").owlCarousel({
        items: 1,
        nav: true,
        dots: true,
        navText: [svg, svg]
    });
}
